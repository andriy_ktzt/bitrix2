<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */






$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');

$menuBlockId = "catalog_menu_".$this->randString();
?>
<div class="nv_topnav">
	<ul >
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>  
			
			<li>
				<a href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>">
					<span><?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?></span>
				</a>
				<?if (is_array($arColumns) && count($arColumns) > 0):?>						
						<?foreach($arColumns as $key=>$arRow):?>
								<ul>
									<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  <!-- second level-->
										<li>
											<a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>" >
												<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?>
											</a>
										</li>
									<?endforeach;?>
								</ul>
						<?endforeach;?>
				<?endif?>
			</li>
		<?endforeach;?>
		<div style="clear: both;"></div>
	</ul>
	
</div>
<?php //echo "<pre/>"; print_r($arResult);?>
 <!-- <div class="nv_topnav"> 
                <ul>
                    <li><a href=""   class="menu-img-fon"  style="background-image: url(/images/nv_home.png);" ><span></span></a></li>
                    <li><a href=""><span>Компания</span></a>
                        <ul>
                            <li><a href="">Пункт 1</a></li>
                            <li><a href="">Пункт 2</a></li>
                            <li><a href="">Пункт 3</a></li>
                            <li><a href="">Пункт 4</a></li>
                        </ul>
                    </li>
                    <li><a href=""><span>Новости</span></a></li>
                    <li><a href=""><span>Каталог</span></a></li>
                    <li><a href=""><span>Акции</span></a>
                        <ul>
                            <li><a href="">Пункт 1</a>
                                <ul>
                                    <li><a href="">Пункт 1</a></li>
                                    <li><a href="">Пункт 2</a></li>
                                </ul>
                            </li>
                            <li><a href="">Пункт 2</a></li>
                            <li><a href="">Пункт 3</a></li>
                            <li><a href="">Пункт 4</a></li>
                        </ul>
                    </li>
                    <li><a href=""><span>Партнерам</span></a></li>
                    <li><a href=""><span>Контакты</span></a></li>
                    <div class="clearboth"></div>
                </ul>
            </div>

 -->

