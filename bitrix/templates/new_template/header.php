<!DOCTYPE HTML>
<html lang="en-US">
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<head>
    <meta charset="utf8">
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead();?>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/template_style.css"/>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/functions.js"></script>

    <!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
</head>
<body>
<div class="wrap">
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div class="hd_header_area">
        <div class="hd_header">
            <table>
                <tr>
                    <td rowspan="2" class="hd_companyname">
                        <?    $url= explode('/',$APPLICATION->GetCurPage());
                                   if ($url[1] == 'products' or $url[1] == 'company')
                                   {
                                           $color = "count_companyname";
                                   }
                        ?>
                        <h1><a href="" class="<?=$color?>">Мебельный магазин</a></h1>
                    </td>
                    <td rowspan="2" class="hd_txarea">
                        <span class="tel">8 (495) 212-85-06</span>	<br/>
                        <?=GetMessage('CFT_TIME')?> <span class="workhours">ежедневно с 9-00 до 18-00</span>
                    </td>
                    <td style="width:232px">
                        <form action="">
                            <div class="hd_search_form" style="float:right;">
                                <input placeholder="Поиск" type="text"/>
                                <input type="submit" value=""/>
                            </div>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 11px;">
							<span class="hd_singin"><a id="hd_singin_but_open" href="">Войти на сайт</a>
							<div class="hd_loginform">
                                <span class="hd_title_loginform">Войти на сайт</span>
                                <form name="" method="" action="">

                                    <input placeholder="Логин"  type="text">
                                    <input  placeholder="Пароль"  type="password">
                                    <a href="/" class="hd_forgotpassword">Забыли пароль</a>

                                    <div class="head_remember_me" style="margin-top: 10px">
                                        <input id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" type="checkbox">
                                        <label for="USER_REMEMBER_frm" title="Запомнить меня на этом компьютере">Запомнить меня</label>
                                    </div>
                                    <input value="Войти" name="Login" style="margin-top: 20px;" type="submit">
                                </form>
                                <span class="hd_close_loginform">Закрыть</span>
                            </div>
							</span><br>
                        <a href="" class="hd_signup">Зарегистрироваться</a>
                    </td>
                </tr>
            </table>          
                <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "catalog_horizontal_old1",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "2",
                    "MENU_CACHE_GET_VARS" => array(0=>"",),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_THEME" => "red",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "N"
                )
            );?>
        </div>
    </div>

    <!--- // end header area --->
    <div class="bc_breadcrumbs">
        <ul>
            <li><a href="">Каталог</a></li>
            <li><a href="">Мебель</a></li>
            <li><a href="">Выставки и события</a></li>
        </ul>
        <div class="clearboth"></div>
    </div>
    <div class="main_container page">
        <div class="mn_container">
            <div class="mn_content">
                <div class="main_post">
                    <div class="main_title">
                        <h1>Заголовок страницы</h1>
                    </div>